# DWM

Blake Boudreaux's build of [dwm (dynamic window manager)](https://dwm.suckless.org). This is version 6.2 of dwm. As of writing this 6.2 is the most recent version of dwm.

This build of dwm is heavily patched. It has the following patches:

- actual fullscreen
- bar height
- cfacts
- vanity gaps (the version of vanity to be installed on top of cfacts)
- colorbar
- cycle layouts
- focus on click
- hide vacant tags
- movestack
- pertag
- reorganize
- resetlayout
- scratchpads
- smartborders
- status2d
- sticky
- underline tags (this patch has been slightly modified so that the underline is a different color than the text)
- Window Swallowing

# Hotkeys

There are far too many hotkeys for me to list here, so I will just list the most important. For the following, note that the "super" key is also known as the "windows" key (it is the key with the windows logo on it).

- super + enter launches a terminal (it is assumed that you have st installed)
- super + d launches dmenu
- super + q to close a program
- super + w launches a web browser (it is assumed you have vimb installed)
- super + shift + w launches an alternative web browser (It is assumed you have chromium installed, though I technically use ungoogled-chromium)
- super + e launches neomutt
- super + shift + e launches calcurse
- super + grave reorganizes tags. For an explanation of what this does [click here](https://dwm.suckless.org/patches/reorganizetags). The "grave" key is the key with tilde on the left of the "1" key on most keyboards

The rest of the hotkeys should be reminiscent of [Luke Smith's build](https://github.com/LukeSmithxyz/dwm) if you are familiar. Either way, the hotkeys listed above should me enough to get around and change everything to your liking.

# Fonts

I should mention that this build expects you to have the "Ubuntu" font, as well as "Font Awesome" (v4.7.0) installed on your system. Of couse this can be easily changed by editing the config.def.h (then copying it to config.h and rebuilding dwm via `sudo make install`).

# Install

To install:

In your terminal, navigate to the directory where you would like to put the source files (I use ~/.local/src for this) and type

```
git clone https://gitlab.com/bboudreaux28/dwm
cd dwm
sudo make install
```

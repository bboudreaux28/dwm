/* See LICENSE file for copyright and license details. */

/* Constants */
#define TERMINAL "st"
#define TERMCLASS "St"

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const unsigned int gappih    = 20;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 30;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int focusonwheel       = 0;
static const int user_bh            = 30;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char buttonbar[]       = "";
static const char *fonts[]          = { "Ubuntu:size=12:antialias=true:autohint=true", "Font Awesome:size=16:antialias=true:autohint=true" };
static const char dmenufont[]       = "Font Awesome:size=16";
static const char col_gray1[]       = "#1D1F21";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#FFFFFF";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#770000";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, "#1D1F21"  },
	[SchemeSel]  = { col_gray3, col_gray1, "#268bd2"  },
        [SchemeTagsUnd] = {"#268bd2", col_gray2,  "#1D1F21"  },
	[SchemeStatus]  = { col_gray3, col_gray1,  "#1D1F21"  }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]  = { col_gray3, col_gray2,  "#1D1F21"  }, // Tagbar left selected {text,background,not used but cannot be empty}
    [SchemeTagsNorm]  = { col_gray3, col_gray1,  "#1D1F21"  }, // Tagbar left unselected {text,background,not used but cannot be empty}
    [SchemeInfoSel]  = { col_gray3, col_gray1,  "#1D1F21"  }, // infobar middle  selected {text,background,not used but cannot be empty}
    [SchemeInfoNorm]  = { col_gray3, col_gray1,  "#1D1F21"  }, // infobar middle  unselected {text,background,not used but cannot be empty}
};

typedef struct {
	const char *name;
	const void *cmd;
} Sp;
const char *spcmd1[] = {"st", "-n", "spterm", "-g", "120x34", NULL };
const char *spcmd2[] = {"st", "-n", "spcalc", "-f", "Ubuntu Mono:size=16", "-g", "50x20", "-e", "bc", "-lq", NULL };
const char *spcmd3[] = {"st", "-n", "spfm", "-g", "144x41", "-e", "ranger", NULL };
const char *spcmd4[] = {"keepassxc", NULL };
static Sp scratchpads[] = {
	/* name          cmd  */
	{"spterm",      spcmd1},
	{"spcalc",      spcmd2},
	{"spranger",    spcmd3},
	{"keepassxc",   spcmd4},
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const unsigned int ulinepad	= 5;	/* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 3;	/* thickness / height of the underline */
static const unsigned int ulinevoffset	= 0;	/* how far above the bottom of the bar the line should appear */
static const int ulineall 		= 0;	/* 1 to show underline on all tags, 0 for just the active ones */

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance                   title           tags mask        isfloating       isterminal       noswallow      monitor */
	{ "dota2",	  NULL,			NULL,		0,		 1,	          0,	           0,			-1 },
	{ "steam_app_393380",	  NULL,			NULL,		0,		 1,	          0,	           0,			-1 },
	{ "squadgame.exe",	  NULL,			NULL,		0,		 1,	          0,	           0,			-1 },
	{ "Albion-Online",	  NULL,			NULL,		0,		 1,	          0,	           0,			-1 },
	{ "contra_rc.exe",	  NULL,			NULL,		0,		 1,	          0,	           0,			-1 },
	{ "deadspace3.exe",	  NULL,			NULL,		0,		 1,	          0,	           0,			-1 },
	{ "steam_app_1182480",	  NULL,			NULL,		0,		 1,	          0,	           0,			-1 },
	{ "origin.exe",	  NULL,			NULL,		0,		 1,	          0,	           0,			-1 },
	{ "reliccoh.exe",	  NULL,			NULL,		0,		 1,	          0,	           0,			-1 },
	{ "Gimp",	  NULL,			NULL,		0,		 1,	          0,	           0,			-1 },
	{ "st-256color",      NULL,                 NULL,           0,               0,               1,               0,                   -1 },
	{ NULL,		  "spterm",		NULL,		SPTAG(0),	 1,		  1,		   0,			-1 },
	{ NULL,           NULL,                 "Event Tester", 0,               0,               0,               1,                   -1 },
	{ NULL,		  "spcalc",		NULL,		SPTAG(1),	 1,		  1, 		   0,			-1 },
	{ NULL,		  "keepassxc",	        NULL,		SPTAG(2),	 0,		  0, 		   0,			-1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"
#include "shiftview.c"
#include <X11/XF86keysym.h>

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "Tall",      tile },    /* first entry is default */
	{ "Mirror Tall",      bstack },
	{ "Columns",      centeredmaster },
	{ "Rows",      bstackhoriz },
	{ "Grid",      nrowgrid },
	{ "Fibonacci",      spiral },
	{ "Dwindle",     dwindle },
	{ "Deck",      deck },
	/*{ "HHH",      grid },*/
	/*{ "---",      horizgrid },*/
	/*{ ":::",      gaplessgrid },*/
	/*{ ">M>",      centeredfloatingmaster },*/
	{ "Monocle",      monocle },
	{ "Float",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };

#include <X11/XF86keysym.h>
#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_d,      spawn,          SHCMD("dmenu_run -h 30 -i -p 'Run:'") },
	{ MODKEY|ShiftMask,             XK_d,      spawn,          SHCMD("ripcord") },
	{ MODKEY|ShiftMask,             XK_s,      spawn,          SHCMD("mpc stop") },
	{ MODKEY,                       XK_w,      spawn,          SHCMD("dmenuvimb") },
	/*{ MODKEY,                       XK_w,      spawn,          SHCMD("dmenu_websearch") },*/
	{ MODKEY|ShiftMask,             XK_w,      spawn,          SHCMD("export $(dbus-launch); dmenu_websearch") },
	{ MODKEY,                       XK_e,      spawn,          SHCMD("st -e neomutt && pkill -RTMIN+1 dwmblocks ") },
	{ MODKEY|ShiftMask,             XK_e,      spawn,          SHCMD("st -e calcurse -D /home/blakeb/.config/calcurse/") },
	{ MODKEY,                       XK_r,      spawn,          SHCMD("st -e lf") },
	{ MODKEY|ShiftMask,             XK_r,      spawn,          SHCMD("st -e htop") },
	{ MODKEY,                       XK_m,      spawn,          SHCMD("st -e ncmpcpp") },
	{ MODKEY,                       XK_n,      spawn,          SHCMD("st -f 'Ubuntu Mono:size=16' -e nvim -c VimwikiIndex") },
	{ MODKEY|ShiftMask,             XK_n,      spawn,          SHCMD("st -f 'Ubuntu Mono:size=16' -e newsboat") },
	{ MODKEY|ShiftMask,             XK_c,      spawn,          SHCMD("st -e reflex-curses") },
	{ MODKEY,                       XK_c,      spawn,          SHCMD("signal-desktop") },
	{ MODKEY|ShiftMask,             XK_equal,  spawn,          SHCMD("pamixer --allow-boost -i 15; kill -36 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_equal,  spawn,          SHCMD("pamixer --allow-boost -i 5; kill -36 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_minus,  spawn,          SHCMD("pamixer --allow-boost -d 5; kill -36 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,             XK_minus,  spawn,          SHCMD("pamixer --allow-boost -d 15; kill -36 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,             XK_m,      spawn,          SHCMD("pamixer -t; kill -36 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_BackSpace, spawn,       SHCMD("sysact") },
	{ MODKEY,			XK_bracketleft,		spawn,		SHCMD("mpc seek -10") },
	{ MODKEY|ShiftMask,		XK_bracketleft,		spawn,		SHCMD("mpc seek -60") },
	{ MODKEY,			XK_bracketright,	spawn,		SHCMD("mpc seek +10") },
	{ MODKEY|ShiftMask,		XK_bracketright,	spawn,		SHCMD("mpc seek +60") },
	{ MODKEY,                       XK_period, spawn,          SHCMD("mpc next") },
	{ MODKEY|ShiftMask,             XK_period, spawn,          SHCMD("mpc repeat") },
	{ MODKEY,                       XK_comma,  spawn,          SHCMD("mpc prev") },
	{ MODKEY|ShiftMask,             XK_comma,  spawn,          SHCMD("mpc seek 0%") },
	{ MODKEY,                       XK_p,      spawn,          SHCMD("mpc toggle") },
	{ 0,                            XK_Print,  spawn,          SHCMD("maim pick-full-$(date '+%y%m%d-%H%m-%S').png") },
	{ ShiftMask,                    XK_Print,  spawn,          SHCMD("maimpick") },
	{ MODKEY,                       XK_F4,     spawn,          SHCMD("st -e pulsemixer") },
	{ MODKEY|ShiftMask,             XK_F4,     spawn,          SHCMD("st -e alsamixer" ) },
	/*{ MODKEY|ShiftMask,             XK_F4,     spawn,          SHCMD("dmenubluetooth" ) },*/
	{ MODKEY,			XK_Print,	spawn,		SHCMD("dmenurecord") },
	{ MODKEY|ShiftMask,		XK_Print,	spawn,		SHCMD("dmenurecord kill") },
	{ MODKEY,			XK_Delete,	spawn,		SHCMD("dmenurecord kill") },
	{ MODKEY|ShiftMask,             XK_p,     spawn,          SHCMD("passmenu -h 30 -i -p 'Password for What?' && notify-send -t 3000 ' Password Copied to Clipboard!'") },
	{ MODKEY,                       XK_F5,     spawn,          SHCMD("mullvad-gui") },
	/*{ MODKEY,                       XK_F5,     spawn,          SHCMD("pia-client") },*/
	{ MODKEY,			XK_F6,	   spawn,	   SHCMD("torwrap") },
	{ MODKEY,                       XK_F7,     spawn,          SHCMD("td-toggle") },
	{ MODKEY,                       XK_F8,     spawn,          SHCMD("checkmail") },
        { MODKEY,			XK_F9,		spawn,		SHCMD("dmenumount") },
	{ MODKEY,			XK_F10,		spawn,		SHCMD("dmenuumount") },
	{ MODKEY,			XK_F11,		spawn,		SHCMD("mpv --no-cache --no-osc --profile=low-latency --force-seekable=yes /dev/video1") },
	{ 0, XF86XK_AudioMute,		spawn,		SHCMD("pamixer -t; pkill -RTMIN+2 dwmblocks") },
	{ 0, XF86XK_AudioRaiseVolume,	spawn,		SHCMD("pamixer --allow-boost -i 3; pkill -RTMIN+2 dwmblocks") },
	{ 0, XF86XK_AudioLowerVolume,	spawn,		SHCMD("pamixer --allow-boost -d 3; pkill -RTMIN+2 dwmblocks") },
	{ 0, XF86XK_AudioPrev,		spawn,		SHCMD("mpc prev") },
	{ 0, XF86XK_AudioNext,		spawn,		SHCMD("mpc next") },
	{ 0, XF86XK_AudioPause,		spawn,		SHCMD("mpc pause") },
	{ 0, XF86XK_AudioPlay,		spawn,		SHCMD("mpc play") },
	{ 0, XF86XK_AudioStop,		spawn,		SHCMD("mpc stop") },
	{ 0, XF86XK_AudioRewind,	spawn,		SHCMD("mpc seek -10") },
	{ 0, XF86XK_AudioForward,	spawn,		SHCMD("mpc seek +10") },
	{ 0, XF86XK_AudioMedia,		spawn,		SHCMD("st -e ncmpcpp") },
	{ 0, XF86XK_AudioMicMute,	spawn,		SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") },
	{ 0, XF86XK_PowerOff,		spawn,		SHCMD("sysact") },
	{ 0, XF86XK_Calculator,		spawn,		SHCMD("st -e bc -l") },
	{ 0, XF86XK_Sleep,		spawn,		SHCMD("sudo -A zzz") },
	{ 0, XF86XK_WWW,		spawn,		SHCMD("vimbdmenu") },
	{ 0, XF86XK_DOS,		spawn,		SHCMD("vimb") },
	{ 0, XF86XK_ScreenSaver,	spawn,		SHCMD("slock & xset dpms force off; mpc pause; pauseallmpv") },
	{ 0, XF86XK_TaskPane,		spawn,		SHCMD("st -e htop") },
	{ 0, XF86XK_Mail,		spawn,		SHCMD("st -e neomutt ; pkill -RTMIN+12 dwmblocks") },
	{ 0, XF86XK_MyComputer,		spawn,		SHCMD("st -e lf /") },
	/* { 0, XF86XK_Battery,		spawn,		SHCMD("") }, */
	{ 0, XF86XK_Launch1,		spawn,		SHCMD("xset dpms force off") },
	{ 0, XF86XK_TouchpadToggle,	spawn,		SHCMD("(synclient | grep 'TouchpadOff.*1' && synclient TouchpadOff=0) || synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOff,	spawn,		SHCMD("synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOn,		spawn,		SHCMD("synclient TouchpadOff=0") },
	{ 0, XF86XK_MonBrightnessUp,	spawn,		SHCMD("xbacklight -inc 15") },
	{ 0, XF86XK_MonBrightnessDown,	spawn,		SHCMD("xbacklight -dec 15") },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_semicolon,  shiftview,      {.i = +1 } },
	{ MODKEY,                       XK_g,      shiftview,      {.i = -1 } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,                       XK_o,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_o,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_h,      setcfact,       {.f = +0.25} },
	{ MODKEY|ShiftMask,             XK_l,      setcfact,       {.f = -0.25} },
	/*{ MODKEY|ShiftMask,             XK_o,      setcfact,       {.f =  0.00} },*/
	{ MODKEY,                       XK_grave,  reorganizetags, {0} },
	{ MODKEY|ShiftMask,             XK_grave,  resetlayout,    {0} },
	{ MODKEY,                       XK_space,  zoom,           {0} },
	{ MODKEY,                       XK_z,      incrgaps,       {.i = +1 } },
	{ MODKEY,                       XK_x,      incrgaps,       {.i = -1 } },
	/*{ MODKEY|Mod4Mask,              XK_i,      incrigaps,      {.i = +1 } },*/
	/*{ MODKEY|Mod4Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } },*/
	/*{ MODKEY|Mod4Mask,              XK_o,      incrogaps,      {.i = +1 } },*/
	/*{ MODKEY|Mod4Mask|ShiftMask,    XK_o,      incrogaps,      {.i = -1 } },*/
	/*{ MODKEY|Mod4Mask,              XK_6,      incrihgaps,     {.i = +1 } },*/
	/*{ MODKEY|Mod4Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },*/
	/*{ MODKEY|Mod4Mask,              XK_7,      incrivgaps,     {.i = +1 } },*/
	/*{ MODKEY|Mod4Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },*/
	/*{ MODKEY|Mod4Mask,              XK_8,      incrohgaps,     {.i = +1 } },*/
	/*{ MODKEY|Mod4Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },*/
	/*{ MODKEY|Mod4Mask,              XK_9,      incrovgaps,     {.i = +1 } },*/
	/*{ MODKEY|Mod4Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },*/
	{ MODKEY,                       XK_a,      togglegaps,     {0} },
	{ MODKEY|ShiftMask,             XK_a,      defaultgaps,    {0} },
	/*{ MODKEY,                       XK_Tab,    view,           {0} },*/
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	/*{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },*/
	/*{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },*/
	{ MODKEY|ShiftMask,		XK_Tab,    cyclelayout,    {.i = -1 } },
	{ MODKEY,                       XK_Tab,    cyclelayout,    {.i = +1 } },
	/*{ MODKEY,                       XK_space,  setlayout,      {0} },*/
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_s,      togglesticky,   {0} },
	{ MODKEY,                       XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	/*{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },*/
	{ MODKEY,                       XK_slash,  focusmon,       {.i = +1 } },
	/*{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },*/
	{ MODKEY|ShiftMask,             XK_slash,  tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,        	XK_Return,  	   togglescratch,  {.ui = 0 } },
	{ MODKEY,            		XK_apostrophe,	   togglescratch,  {.ui = 1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	/*{ MODKEY|ShiftMask,             XK_q,     spawn,          SHCMD("sysact" ) },*/
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkButton,		0,		Button1,	spawn,		SHCMD("actionbtn") },
	{ ClkLtSymbol,          0,              Button1,        cyclelayout,    {.i = +1 } },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

